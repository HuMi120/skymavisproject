using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameState
{
    Init,
    StartGame,
    EndGame
}
public class GameManager : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private RoundController _roundController;
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private CharacterController _characterController;
    [SerializeField] private InputManager _inputManager;
    [SerializeField] private CameraController _cameraController;

    public static GameState GameState = GameState.Init;
    private void Awake()
    {
        _roundController.GenerateRound();
        _inputManager._onCallBackWhenMiddleMouseScrolling += _cameraController.Zoom;
        _inputManager._onCallBackWhenDragging += _cameraController.Drag;
        _uiManager._onNormalSpeedTimeCallBack += NormalSpeedGame;
        _uiManager._onSpeedUpTimeCallBack += SpeedUpGame;
    }
    private void Update()
    {
        StartGame();
        UpdatePower();
        CheckEndGameCondiction();
    }
    private void StartGame()
    {
        if (GameState != GameState.Init) return;
        if (!_roundController.IsFinishGeneratedMap()) return;
        GameState = GameState.StartGame;
        _uiManager.SwitchLoadingPanel(false);
        _uiManager.ShowControlPanel();
        _uiManager.ShowPowerPanel();
    }
    public void CheckEndGameCondiction()
    {
        if (GameState != GameState.StartGame) return;
        var isAttackerAlive = _characterController.IsAttackersAlive();
        var isDefenderAlive = _characterController.IsDefendersAlive();
        if (!isAttackerAlive || !isDefenderAlive)
        {
            _uiManager.ShowFinalResultText(isAttackerAlive ? "ATTACKERS WIN" : "DEFENDERS WIN");
            GameState = GameState.EndGame;
        }
        else if (!isAttackerAlive && !isDefenderAlive)
        {
            _uiManager.ShowFinalResultText("DRAW");
            GameState = GameState.EndGame;
        }
      
    }
    private void NormalSpeedGame()
    {
        Time.timeScale = 1;
    }
    private void SpeedUpGame()
    {
        Time.timeScale = 1.5f;
    }
    private void UpdatePower()
    {
        if (GameState != GameState.StartGame) return;
        _uiManager.ApplyAttackerPower(_characterController.GetAttackPower());
        _uiManager.ApplyDefenderPower(_characterController.GetDefenderPower());

    }
}
