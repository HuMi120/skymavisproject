using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{
  
    public Action<float> _onCallBackWhenMiddleMouseScrolling;
    public Action<Vector3> _onCallBackWhenDragging;
    private Vector3 _lastMousePosition;
    private bool _isHoldingMouseKey = false;
    private Camera _camera;
    private void Awake()
    {
        _camera = Camera.main;
    }
    private void Update()
    {
        if(Input.mouseScrollDelta.y != 0)
        {
            _onCallBackWhenMiddleMouseScrolling?.Invoke(Input.mouseScrollDelta.y);
        }
        if (Input.GetButtonDown("Fire1"))
        {
            _isHoldingMouseKey = true;
            _lastMousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetButtonUp("Fire1"))
        {
            _isHoldingMouseKey = false;
        }
        if(_isHoldingMouseKey)
        {
            _onCallBackWhenDragging?.Invoke(_lastMousePosition - _camera.ScreenToWorldPoint(Input.mousePosition));
            _lastMousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}
