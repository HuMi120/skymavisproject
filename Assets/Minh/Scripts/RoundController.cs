using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RoundController : MonoBehaviour
{
    [Header("Basic Setting")]
    private Vector2 _mapSize = Vector2.zero;
    [SerializeField] private string _mapName = "map1";
    [Header("Init")]
    [SerializeField] private PlayGroundController _groundController;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private CharacterController _characterController;

    private const string MAP_TILE_ID = "0";
    private const string MAP_ATTACKER_ID = "1";
    private const string MAP_DEFENDER_ID = "2";
    public Vector2 GetMapSize() => _mapSize;
    private bool _isFinishGeneratedMap = false;
    public void GenerateRound()
    {
        StartCoroutine(LoadMapFromText(_mapName));
        //_groundController.GenerateMap(_mapSize);
        //_cameraController.SetPosition(_mapSize);
    }
    private IEnumerator LoadMapFromText(string mapName)
    {
        _isFinishGeneratedMap = false;
        var path = Path.Combine(Application.streamingAssetsPath, mapName + ".txt");
        var maxX = 0;
        var maxY = 0;
        using (StreamReader sr = File.OpenText(path))
        {

            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                var data = line.Trim().Split(' ');

                for (int i = 0; i < data.Length; i++)
                {
                    switch (data[i])
                    {
                        case MAP_ATTACKER_ID:
                            _characterController.CreateCharacter(CharacterType.Attacker, new Vector2(i, maxY));

                            break;
                        case MAP_DEFENDER_ID:
                            _characterController.CreateCharacter(CharacterType.Defender, new Vector2(i, maxY));
                            break;
                    }
                    yield return new WaitUntil(() => _characterController.IsFinishCreatingCharacter());
                    _groundController.GenerateTile(i, maxY);
                }

                if (maxX < data.Length)
                {
                    maxX = data.Length;
                }
                maxY++;
            }
        }
        _isFinishGeneratedMap = true;
        _cameraController.SetPosition(maxX, maxY);
    }
    public bool IsFinishGeneratedMap() => _isFinishGeneratedMap;
}
