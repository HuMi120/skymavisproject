using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
public class PathFindingManager : MonoBehaviour
{
    [Header("Basic Setting")]
    [SerializeField] private float _cellSize = 0.1f;
    [Header("Init")]
    [SerializeField] private CharacterController _characterController;

    private static PathFindingManager _instance;
    public static PathFindingManager Instance { get { return _instance; } }

    private void Awake()
    {
        _instance = this;

    }
    public Stack<Vector3> GetPath(MovePathFindingController move, Vector3 original, Vector3 target)
    {
        int distance = (int)((Vector3.Distance(original, target) + 1) / _cellSize);
        var basePostion = FindBasePosition(original, target);
        var allBasicCharacterData = _characterController.GetAllCharactersBasicData();
        var temp = new List<CharacterBasicData>();
        foreach (var basicCharacterData in allBasicCharacterData)
        {
            if ((basicCharacterData._position - original).sqrMagnitude <= 0.01f) continue;
            if ((basicCharacterData._position - target).sqrMagnitude <= 0.01f) continue;
            temp.Add(basicCharacterData);
        }
        move._pathFinding = new Pathfinding(distance, distance, basePostion, _cellSize, temp);
        return move._pathFinding.FindPath(original, target);
    }
    private Vector3 FindBasePosition(Vector3 original, Vector3 target)
    {
        Vector3 leftDown = Vector3.zero;
        leftDown.x = (original.x > target.x ? target.x : original.x);
        leftDown.y = (original.y > target.y ? target.y : original.y);
        return leftDown;
    }
}
