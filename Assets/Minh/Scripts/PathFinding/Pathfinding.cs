﻿using System.Collections.Generic;
using UnityEngine;

public class Pathfinding
{

    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;

    public static Pathfinding Instance { get; private set; }

    private Grid<PathNode> grid;
    private List<PathNode> openList;
    private HashSet<PathNode> closedList;

    public Pathfinding(int width, int height, Vector3 baseCenterPosition, float cellSize, List<CharacterBasicData> listCharacterData)
    {
        Instance = this;
        //baseCenterPosition.x = baseCenterPosition.x - width * cellSize * 0.5f;
        //baseCenterPosition.y = baseCenterPosition.y - height * cellSize * 0.5f;
        grid = new Grid<PathNode>(width, height, cellSize, baseCenterPosition, listCharacterData, (Grid<PathNode> g, int x, int y, List<CharacterBasicData> listCharacterData) => new PathNode(g, x, y, listCharacterData));
    }
    public void SetData(Vector3 baseCenterPosition, List<CharacterBasicData> listCharacterData)
    {
        grid.SetOriginalPosition(baseCenterPosition);
        var gridArray = grid.GetGridArray();
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                gridArray[x, y].SetData(listCharacterData);
            }
        }
    }

    public Grid<PathNode> GetGrid()
    {
        return grid;
    }

    public Stack<Vector3> FindPath(Vector3 startWorldPosition, Vector3 endWorldPosition)
    {
        grid.GetXY(startWorldPosition, out int startX, out int startY);
        grid.GetXY(endWorldPosition, out int endX, out int endY);

        Stack<Vector3> path = FindPath(startX, startY, endX, endY);
        return path;
    }

    public Stack<Vector3> FindPath(int startX, int startY, int endX, int endY)
    {
        PathNode startNode = grid.GetGridObject(startX, startY);
        PathNode endNode = grid.GetGridObject(endX, endY);

        if (startNode == null || endNode == null)
        {
            // Invalid Path
            return null;
        }

        openList = new List<PathNode> { startNode };
        closedList = new HashSet<PathNode>();

        startNode.gCost = 0;
        startNode.hCost = CalculateDistanceCost(startNode, endNode);
        startNode.CalculateFCost();

        PathNode temp = null;
        while (openList.Count > 0)
        {
            PathNode currentNode = GetLowestFCostNode(openList);
            if (temp == null || PathNode.SqrDistance(temp, endNode) > PathNode.SqrDistance(currentNode, endNode))
            {
                temp = currentNode;
            }
            if (currentNode == endNode)
            {
                return CalculatePath(endNode);
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            foreach (PathNode neighbourNode in GetNeighbourList(currentNode))
            {
                if (closedList.Contains(neighbourNode)) continue;
                if (!neighbourNode.isWalkable)
                {
                    closedList.Add(neighbourNode);
                    continue;
                }

                int tentativeGCost = currentNode.gCost + CalculateDistanceCost(currentNode, neighbourNode);
                if (tentativeGCost < neighbourNode.gCost)
                {
                    neighbourNode.cameFromNode = currentNode;
                    neighbourNode.gCost = tentativeGCost;
                    neighbourNode.hCost = CalculateDistanceCost(neighbourNode, endNode);
                    neighbourNode.CalculateFCost();

                    if (!openList.Contains(neighbourNode))
                    {
                        openList.Add(neighbourNode);
                    }
                }
            }
        }
        if (temp != null) return CalculatePath(temp);
        // Out of nodes on the openList
        return null;
    }

    private List<PathNode> GetNeighbourList(PathNode currentNode)
    {
        List<PathNode> neighbourList = new List<PathNode>();
        var tempX = currentNode.x - 1;
        var tempX2 = currentNode.x + 1;
        var tempX3 = currentNode.y - 1;
        var tempX4 = currentNode.y + 1;
        if (tempX >= 0)
        {
            // Left
            neighbourList.Add(GetNode(tempX, currentNode.y));
            // Left Down
            if (tempX3 >= 0) neighbourList.Add(GetNode(tempX, tempX3));
            // Left Up
            if (tempX4 < grid.GetHeight()) neighbourList.Add(GetNode(tempX, tempX4));
        }
        if (tempX2 < grid.GetWidth())
        {
            // Right
            neighbourList.Add(GetNode(tempX2, currentNode.y));
            // Right Down
            if (tempX3 >= 0) neighbourList.Add(GetNode(tempX2, tempX3));
            // Right Up
            if (tempX4 < grid.GetHeight()) neighbourList.Add(GetNode(tempX2, tempX4));
        }
        // Down
        if (tempX3 >= 0) neighbourList.Add(GetNode(currentNode.x, tempX3));
        // Up
        if (tempX4 < grid.GetHeight()) neighbourList.Add(GetNode(currentNode.x, tempX4));

        return neighbourList;
    }

    public PathNode GetNode(int x, int y)
    {
        return grid.GetGridObject(x, y);
    }

    private Stack<Vector3> CalculatePath(PathNode endNode)
    {
        Stack<Vector3> path = new Stack<Vector3>();
        path.Push(grid.GetWorldPosition(endNode.x, endNode.y));
        PathNode currentNode = endNode;
        while (currentNode.cameFromNode != null)
        {
            path.Push(grid.GetWorldPosition(currentNode.cameFromNode.x, currentNode.cameFromNode.y));
            currentNode = currentNode.cameFromNode;
        }
        //path.Reverse();
        return path;
    }

    private int CalculateDistanceCost(PathNode a, PathNode b)
    {
        int xDistance = a.x > b.x ? (a.x - b.x) : (b.x - a.x);
        int yDistance = a.y > b.y ? (a.y - b.y) : (b.y - a.y);
        int remaining = xDistance > yDistance ? (xDistance - yDistance) : (yDistance - xDistance);
        int temp = xDistance > yDistance ? yDistance : xDistance;
        return MOVE_DIAGONAL_COST * temp + MOVE_STRAIGHT_COST * remaining;
    }

    private PathNode GetLowestFCostNode(List<PathNode> pathNodeList)
    {
        PathNode lowestFCostNode = pathNodeList[0];
        for (int i = 1; i < pathNodeList.Count; i++)
        {
            if (pathNodeList[i].fCost < lowestFCostNode.fCost)
            {
                lowestFCostNode = pathNodeList[i];
            }
        }
        return lowestFCostNode;
    }

}
