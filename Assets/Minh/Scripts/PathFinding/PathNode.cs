﻿using System.Collections.Generic;
using UnityEngine;

public class PathNode
{

    private Grid<PathNode> grid;
    public int x;
    public int y;

    public int gCost;
    public int hCost;
    public int fCost;

    public bool isWalkable = true;
    public PathNode cameFromNode;

    public PathNode(Grid<PathNode> grid, int x, int y, List<CharacterBasicData> characterBasicDatas)
    {
        this.grid = grid;
        this.x = x;
        this.y = y;
        isWalkable = true;
        for (int i = 0; i < characterBasicDatas.Count; i++)
        {
            CheckCanWalk(characterBasicDatas[i]._position, characterBasicDatas[i]._range);
        }
        gCost = 99999999;
        CalculateFCost();
        cameFromNode = null;
    }
    public void SetData(List<CharacterBasicData> characterBasicDatas)
    {
        isWalkable = true;
        for (int i = 0; i < characterBasicDatas.Count; i++)
        {
            CheckCanWalk(characterBasicDatas[i]._position, characterBasicDatas[i]._range);
        }
        gCost = 99999999;
        CalculateFCost();
        cameFromNode = null;
    }

    public void CalculateFCost()
    {
        fCost = gCost + hCost;
    }

    public void SetIsWalkable(bool isWalkable)
    {
        this.isWalkable = isWalkable;
    }
    public void CheckCanWalk(Vector3 position, float range)
    {
        if (!isWalkable) return;
        var tempVector = grid.GetWorldPosition(x, y);
        var tempDis = range + grid.GetCellSize() / 2f;
        tempDis = tempDis * tempDis;

        if ((tempVector - position).sqrMagnitude <= tempDis)
        {
            isWalkable = false;
        }
    }
    public bool IsWalkable() => isWalkable;

    public override string ToString()
    {
        return x + "," + y;
    }
    public static long SqrDistance(PathNode a, PathNode b)
    {
        var x = a.x - b.x;
        x = x * x;
        var y = a.y - b.y;
        y = y * y;
        return x + y;
    }
}
