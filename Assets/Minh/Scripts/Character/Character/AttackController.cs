using Spine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private AnimationController _animation;
    private IEnumerator _attackIE;
    private bool _isAttacking = false;
    private Character _character;
    private int _basicDamage;
    private void Awake()
    {
        _animation._onCallBackAttackEnd += MakeDamage;
    }
    public void Attack(Character character, int basicDamage, float attackSpeed)
    {
        _character = character;
        if (_isAttacking) return;
        _isAttacking = true;


        _basicDamage = basicDamage;
        _attackIE = IEAttack(attackSpeed);
        StartCoroutine(_attackIE);
    }
    public void StopAttacking()
    {
        if (_attackIE != null) StopCoroutine(_attackIE);
        _isAttacking = false;
    }
    private IEnumerator IEAttack(float attackSpeed)
    {
        WaitForSeconds wait = new WaitForSeconds(attackSpeed);
        while (true)
        {
            _animation.PlayAttack();
            yield return wait;
        }
    }
    private void MakeDamage()
    {
        if (_character != null)
        {
            _character.TakeDamage(_basicDamage + RandomDamage(GetRandomDamage(), _character.GetRandomDamage()));
        } 
    }
    public int GetRandomDamage()
    {
        return Random.Range(0, 3);
    }
    private int RandomDamage(int selfRandom, int targetRandom)
    {
        var num = (3 + selfRandom - targetRandom) % 3;
        return num == 0 ? 1 : num == 1 ? 2 : 0;
    }
}
