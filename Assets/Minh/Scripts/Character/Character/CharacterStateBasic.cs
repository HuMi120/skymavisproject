using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "CustomData/CharacterState")]
public class CharacterStateBasic : ScriptableObject
{
    [Header("State")]
    public int _health;
    public float _speed;
    public float _attackRange;
    public int _damage;
    public float _attackSpeed;
    public float _size;
}
