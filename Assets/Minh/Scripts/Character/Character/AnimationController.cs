using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationController : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private SkeletonAnimation _skeletonAnimation;
    [Header("Basic Setting")]
    [SerializeField] private string _idleAnim = "action/idle/normal";
    [SerializeField] private string _walkAnim = "action/move-forward";
    [SerializeField] private string _attackAnim = "attack/melee/normal-attack";
    private string _currentAnimation = "";
    public Action _onCallBackIdleEnd;
    public Action _onCallBackWalkEnd;
    public Action _onCallBackAttackEnd;
    public SkeletonAnimation SkeletonAnimation { get { return _skeletonAnimation; } }
    public void Init()
    {
        _skeletonAnimation.AnimationState.End += HandleEvent;
    }
    public void PlayIdle()
    {
        PlayAnim(_idleAnim, true);
    }
    public void PlayWalk()
    {
        PlayAnim(_walkAnim, true);
    }
    public void PlayAttack()
    {
        PlayAnim(_attackAnim, false);
    }
    private void PlayAnim(string animName, bool isLoop)
    {
        if (!_currentAnimation.Equals(animName))
        {
            _skeletonAnimation.state.SetAnimation(0, animName, isLoop);
            _currentAnimation = animName;
        }
        else
        {
            if (!isLoop)
            {
                _skeletonAnimation.state.SetAnimation(0, animName, isLoop);
            }
        }
        
    }
    private void HandleEvent(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name.Equals(_idleAnim))
        {
            _onCallBackIdleEnd?.Invoke();
        }else if (trackEntry.Animation.Name.Equals(_walkAnim))
        {
            _onCallBackWalkEnd?.Invoke();
        }else if (trackEntry.Animation.Name.Equals(_attackAnim))
        {
            _onCallBackAttackEnd?.Invoke();
        }

    }
}
