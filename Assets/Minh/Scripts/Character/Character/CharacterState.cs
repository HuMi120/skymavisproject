using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterState : MonoBehaviour
{
    [Header("Basic Setting")]
    [SerializeField] private CharacterStateBasic _attackState;
    [SerializeField] private CharacterStateBasic _defendeState;
    [SerializeField] private float _updateHealthBarTime = 0.5f;
    [SerializeField] private Color _maxHealthColor = Color.green;
    [SerializeField] private Color _minHealthColor = Color.red;
    [Header("Init")]
    [SerializeField] private Slider _healthBar;
    [SerializeField] private Image _healthFillBar;
    private int _health;
    private int _maxHealth;
    private float _speed;
    private float _attackRange;
    private int _damage;
    private float _attackSpeed;
    private float _size;
    private float _updateHealthBarSpeed = 0;
    private float _currentHealth;
    private float _finalHealth;
    public void SetTypeCharacter(CharacterType characterType)
    {
        var basicState = characterType == CharacterType.Attacker ? _attackState : _defendeState;
        _health = basicState._health;
        _maxHealth = basicState._health;
        _speed = basicState._speed;
        _attackRange = basicState._attackRange;
        _damage = basicState._damage;
        _attackSpeed = basicState._attackSpeed;
        _size = basicState._size;
        _healthBar.maxValue = _health;
        _healthBar.value = _health;
        _currentHealth = _health;
        _finalHealth = _health;
        _healthFillBar.color = _maxHealthColor;
    }
    private void Update()
    {
        UpdateHealthBar();
    }
    public bool IsAlive() => _health > 0;
    public float Speed { get { return _speed; } }
    public float AttackRange { get { return _attackRange; } }
    public int Damage { get { return _damage; } }
    public float AttackSpeed { get { return _attackSpeed; } }
    public float Size { get { return _size; } }
    public int Health { get { return _health; } }
    public int MaxHealth { get { return _maxHealth; } }
    public void TakeDamage(int damage)
    {
        _currentHealth = _health;
        _health -= damage;
        _finalHealth = _health;
        if(_health <= 0)
        {
            _health = 0;
            _finalHealth = 0;
            gameObject.SetActive(false);
        }
        //_healthBar.value = _health;
        _updateHealthBarSpeed = (_currentHealth - _finalHealth) / _updateHealthBarTime;
    }
    private void UpdateHealthBar()
    {
        if(_currentHealth > _finalHealth + 0.1f)
        {
            _currentHealth -= _updateHealthBarSpeed * Time.deltaTime;
            _healthBar.value = _currentHealth;
            _healthFillBar.color = (_maxHealthColor - _minHealthColor) * _healthBar.value / _healthBar.maxValue + _minHealthColor;
        }
    }
}
