using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePathFindingController : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private AnimationController _anim;
    [SerializeField] private Transform _body;
    [Header("Basic Setting")]
    [SerializeField] private float _waitTimeFindPath = 0.3f;
    private Transform _transform;
    private Stack<Vector3> _tempPath;
    private IEnumerator _loopPathIE;
    public Pathfinding _pathFinding;
    private bool _isWaitingForResult = false;
    private WaitForSeconds _wait;
    private IEnumerator _countDownIE;

    private void Awake()
    {
        _transform = transform;
        _wait = new WaitForSeconds(_waitTimeFindPath);
        _countDownIE = CountDownNextGetFindPath();
    }
    public Vector3 GetPosition() => _transform.position;
    public void SetPosition(Vector3 position) => _transform.position = position;

    public void MoveToTarget(Vector3 target, float speed, float distance)
    {
        FaceTarget(target);
        Move(target, speed, distance);
      
    }
    public void Move(Vector3 target, float speed, float distance)
    {
        if (_isWaitingForResult) return;
        if (speed <= 0) return;
        StartCoroutine(_countDownIE);
        if (_loopPathIE != null) StopCoroutine(_loopPathIE);
        _loopPathIE = LoopPath(target, speed, distance);
        StartCoroutine(_loopPathIE);
    }
    private IEnumerator CountDownNextGetFindPath()
    {
        _isWaitingForResult = true;
        yield return _wait;
        _isWaitingForResult = false;

    }
    private IEnumerator LoopPath(Vector3 target, float speed, float distance)
    {
        _tempPath = PathFindingManager.Instance.GetPath(this, GetPosition(), target);
        if (_tempPath != null)
        {
            while (_tempPath.Count > 0)
            {
                var tempPosition = _tempPath.Pop();
                while ((GetPosition() - tempPosition).sqrMagnitude > 0.015f)
                {
                    _transform.Translate((tempPosition - GetPosition()).normalized * speed * Time.deltaTime);
                    if ((GetPosition() - target).sqrMagnitude <= distance * distance)
                    {
                        _anim.PlayIdle();
                        if (_loopPathIE != null) StopCoroutine(_loopPathIE);
                    }

                    _anim.PlayWalk();
                    yield return null;
                }

            }
            _anim.PlayIdle();
        }

    }
    private void OnDisable()
    {
        StopMoving();
    }
    private void OnDestroy()
    {
        StopMoving();
    }
    private void FaceTarget(Vector3 target)
    {
        var bodyScale = _body.localScale;
        bodyScale.x = (target.x > GetPosition().x ? -1 : 1) * bodyScale.y;
        _body.localScale = bodyScale;
    }
    public void StopMoving()
    {
        if (_loopPathIE != null) StopCoroutine(_loopPathIE);
        if (_countDownIE != null) StopCoroutine(_countDownIE);
        _isWaitingForResult = false;
    }
}
