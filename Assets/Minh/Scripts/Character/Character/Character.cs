using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private AnimationController _animation;
    [SerializeField] private MovePathFindingController _move;
    [SerializeField] private CharacterState _state;
    [SerializeField] private AttackController _attack;
    private Character _target;
    private BasicPlayerManager _basicPlayerManager;
    public Character Init()
    {
        _animation.Init();
        return this;
    }
    public Character SetCharacterType(CharacterType characterType)
    {
        _state.SetTypeCharacter(characterType);
        return this;
    }
    public Character SetManager(BasicPlayerManager basicPlayerManager)
    {
        _basicPlayerManager = basicPlayerManager;
        return this;
    }
    public Character SetPosition(Vector3 position)
    {
        _move.SetPosition(position);
        return this;
    }
    public void SetTarget(Character character) => _target = character;
    public SkeletonAnimation GetSkeletonAnimation() => _animation.SkeletonAnimation;
    public Vector3 GetPosition() => _move.GetPosition();
    public bool IsAlive() => _state.IsAlive();
    public float GetSize() => _state.Size;
    public int GetRandomDamage() => _attack.GetRandomDamage();
    public int GetHealth() => _state.Health;
    public int GetMaxHealth() => _state.MaxHealth;
    public bool CheckReachTarget(Vector3 target, float distance) => (GetPosition() - target).sqrMagnitude <= distance * distance;
    private void Update()
    {
        if (GameManager.GameState != GameState.StartGame)
        {
            Idle();
            return;
        }
        if (_target == null || !_target.IsAlive())
        {
            FindTarget();
        }
        if (_target != null && _target.IsAlive() && IsAlive())
        {
            if (!CheckReachTarget(_target.GetPosition(), _state.AttackRange))
            {
                Move();
            }

            else
            {
                Attack();
            }
        }
        else
        {
            Idle();
        }
    }
    private void FindTarget()
    {
        _target = _basicPlayerManager.GetNearestCharacter(GetPosition());
    }
    private void Move()
    {
        _attack.StopAttacking();
        _move.MoveToTarget(_target.GetPosition(), _state.Speed, _state.AttackRange); ;
    }
    public void TakeDamage(int damage)
    {
        _state.TakeDamage(damage);
    }
    private void Attack()
    {
        _move.StopMoving();
        _attack.Attack(_target, _state.Damage, _state.AttackSpeed);
    }
    private void Idle()
    {
        _animation.PlayIdle();
        _move.StopMoving();
        _attack.StopAttacking();
    }
}
