using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    private Transform _transform;
    private void Awake()
    {
        _transform = transform;
    }
    public Vector3 GetPosition() => _transform.position;
    public void SetPosition(Vector3 position) => _transform.position = position;
    public bool MoveToTarget(Vector3 target, float speed, float distance)
    {
        //Debug.Log("moving");
        var temp = target - _transform.position;
        if (temp.sqrMagnitude > distance)
        {
            _transform.Translate(temp.normalized * speed * Time.deltaTime);
            return false;
        }
        return true;
    }
}
