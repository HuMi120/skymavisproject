using AxieMixer.Unity;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum CharacterType
{
    Attacker,
    Defender
}
public class CharacterBasicData
{
    public Vector3 _position;
    public float _range;
    public CharacterBasicData(Vector3 position, float range)
    {
        _position = position;
        _range = range;
    }
}
public class CharacterController : MonoBehaviour
{
    [Header("Basic Setting")]
    [SerializeField] private string _attackerCharacterName;
    [SerializeField] private string _defenderCharacterName;
    [Header("Init")]
    [SerializeField] private BasicPlayerManager _attackTeamController;
    [SerializeField] private BasicPlayerManager _defenderTeamController;
    [SerializeField] private Character _basicCharacter;
    private Queue<Character> _poolCharacter = new Queue<Character>();
    private bool _isFinishCreatingCharacter = true;
    private void Awake()
    {
        Mixer.Init();
    }
    public bool IsAttackersAlive()
    {
        return !_attackTeamController.IsAllCharacterDead();
    }
    public bool IsDefendersAlive()
    {
        return !_defenderTeamController.IsAllCharacterDead();
    }
    public void CreateCharacter(CharacterType characterType, Vector2 position)
    {
       
        StartCoroutine(
            SpawnAxiesGenes(
                characterType == CharacterType.Attacker ? _attackerCharacterName : _defenderCharacterName,
                characterType,
                position));
    }
    public IEnumerator SpawnAxiesGenes(string axieId, CharacterType characterType, Vector2 position)
    {
        _isFinishCreatingCharacter = false;
        string searchString = "{ axie (axieId: \"" + axieId + "\") { id, genes, newGenes}}";
        JObject jPayload = new JObject();
        jPayload.Add(new JProperty("query", searchString));

        var wr = new UnityWebRequest("https://graphql-gateway.axieinfinity.com/graphql", "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(jPayload.ToString().ToCharArray());
        wr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        wr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        wr.SetRequestHeader("Content-Type", "application/json");
        wr.timeout = 10;
        yield return wr.SendWebRequest();
        if (wr.error == null)
        {
            var result = wr.downloadHandler != null ? wr.downloadHandler.text : null;
            if (!string.IsNullOrEmpty(result))
            {
                JObject jResult = JObject.Parse(result);
                string genesStr = (string)jResult["data"]["axie"]["newGenes"];
                var masterList = characterType == CharacterType.Attacker ? _attackTeamController : _defenderTeamController;
                var character = SpawnCharacter(characterType == CharacterType.Attacker ?
                    _attackTeamController.GetContainer() :
                    _defenderTeamController.GetContainer());
                Mixer.SpawnSkeletonAnimation(character.GetSkeletonAnimation(), axieId, genesStr);
                character.Init().SetPosition(position).SetCharacterType(characterType).SetManager(masterList);
                masterList.AddCharacter(character);
            }
        }
        _isFinishCreatingCharacter = true;
    }
    private Character SpawnCharacter(Transform container)
    {
        Character character = _poolCharacter.Count > 0 ? _poolCharacter.Dequeue() : Instantiate(_basicCharacter, container);
        character.gameObject.SetActive(true);
        return character;

    }
    public bool IsFinishCreatingCharacter() => _isFinishCreatingCharacter;
    public List<CharacterBasicData> GetAllCharactersBasicData()
    {
        List<CharacterBasicData> result = new List<CharacterBasicData>();
        foreach(var character in _attackTeamController.GetCharacters())
        {
            if (!character.IsAlive()) continue;
            result.Add(new CharacterBasicData(character.GetPosition(), character.GetSize()));
        }
        foreach (var character in _defenderTeamController.GetCharacters())
        {
            if (!character.IsAlive()) continue;
            result.Add(new CharacterBasicData(character.GetPosition(), character.GetSize()));
        }
        return result;
    }
    public float GetAttackPower()
    {
        return _attackTeamController.GetPower();
    }
    public float GetDefenderPower()
    {
        return _defenderTeamController.GetPower();
    }
}
