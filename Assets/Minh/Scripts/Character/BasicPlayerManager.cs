using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPlayerManager : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private BasicPlayerManager _otherBasicPlayerManager;
    [SerializeField] private Transform _container;
    private List<Character> _characterList = new List<Character>();
    public void AddCharacter(Character character)
    {
        _characterList.Add(character);
    }
    public void ResetCharacterList()
    {
        foreach (var character in _characterList)
        {
            Destroy(character);
        }
        _characterList.Clear();
    }
    public Character GetNearestCharacter(Vector3 position)
    {
        Character character = null;
        float distance = 0f;
        for (int i = 0; i < _otherBasicPlayerManager._characterList.Count; i++)
        {
            var tempCharacter = _otherBasicPlayerManager._characterList[i];
            if (!tempCharacter.IsAlive()) continue;
            if (character == null || (tempCharacter.GetPosition() - position).sqrMagnitude < distance * distance)
            {
                character = tempCharacter;
                distance = Vector3.Distance(position, tempCharacter.GetPosition());
            }
        }
        return character;
    }
    public bool IsAllCharacterDead()
    {
        foreach(var character in _characterList)
        {
            if (character.IsAlive()) return false;
        }
        return true;
    }
    public Transform GetContainer() => _container;
    public List<Character> GetCharacters() => _characterList;
    public float GetPower()
    {
        int totalHealth = 0, totalMaxHealth = 0;
        foreach(var character in _characterList)
        {
            totalHealth += character.GetHealth();
            totalMaxHealth += character.GetMaxHealth();
        }
        return totalHealth * 1f / totalMaxHealth;
    }
}
