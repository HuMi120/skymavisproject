using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayGroundController : MonoBehaviour
{
    [Header("Basic Setting")]
    [SerializeField] private Color _whiteColor;
    [SerializeField] private Color _blackColor;
    [Header("Init")]
    [SerializeField] private SpriteRenderer _tile;
    [SerializeField] private Transform _container;
    private Queue<SpriteRenderer> _poolTile = new Queue<SpriteRenderer>();
    public void GenerateMap(Vector2 mapSize)
    {
        for (int i = 0; i < mapSize.x; i++)
        {
            for (int j = 0; j < mapSize.y; j++)
            {
                var tile = SpawnTile();
                tile.transform.position = new Vector2(i, j);
                if (i % 2 == 0)
                {
                    tile.color = j % 2 == 0 ? _whiteColor : _blackColor;
                }
                else
                {
                    tile.color = j % 2 != 0 ? _whiteColor : _blackColor;
                }
            }
        }
    }
    public void GenerateTile(int x, int y)
    {
        var tile = SpawnTile();
        tile.gameObject.SetActive(true);
        tile.transform.position = new Vector2(x, y);
        if (x % 2 == 0)
        {
            tile.color = y % 2 == 0 ? _whiteColor : _blackColor;
        }
        else
        {
            tile.color = y % 2 != 0 ? _whiteColor : _blackColor;
        }
    }
    private SpriteRenderer SpawnTile()
    {
        SpriteRenderer title = _poolTile.Count > 0 ? _poolTile.Dequeue() : Instantiate(_tile, _container);
        title.gameObject.SetActive(true);
        return title;
    }
}
