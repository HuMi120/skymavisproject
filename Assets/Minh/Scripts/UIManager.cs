using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Init")]
    [SerializeField] private GameObject _loadingPanel;
    [SerializeField] private TMP_Text _finalResultText;
    [SerializeField] private GameObject _controlPanel;
    [SerializeField] private GameObject _powerPanel;
    [SerializeField] private Slider _attackPower;
    [SerializeField] private Slider _defenderPower;
    [Header("Basic Setting")]
    [SerializeField] private float _updatePowerSpeed = 0.2f;
    public Action _onNormalSpeedTimeCallBack;
    public Action _onSpeedUpTimeCallBack;
    private float _currentAttackPower;
    private float _finalAttackPower;
    private float _currentDefenderPower;
    private float _finalDefenderPower;
    private float _updateAttackPowerSpeed;
    private float _updateDefendPowerSpeed;
    private void Awake()
    {
        _currentAttackPower = _attackPower.maxValue;
        _finalAttackPower = _attackPower.maxValue;
        _currentDefenderPower = _defenderPower.maxValue;
        _finalDefenderPower = _defenderPower.maxValue;
    }
    public void SwitchLoadingPanel(bool turnOn = false)
    {
        _loadingPanel.SetActive(turnOn);
    }
    public void ShowFinalResultText(string finalResult)
    {
        _finalResultText.gameObject.SetActive(true);
        _finalResultText.text = finalResult;
    }
    public void OnNormalSpeedTimeCallBack()
    {
        _onNormalSpeedTimeCallBack?.Invoke();
    }
    public void OnSpeedUpTimeCallBack()
    {
        _onSpeedUpTimeCallBack?.Invoke();
    }
    public void ShowControlPanel()
    {
        _controlPanel.SetActive(true);
    }
    public void ApplyAttackerPower(float value)
    {
        _currentAttackPower = _attackPower.value;
        _finalAttackPower = value;
        _updateAttackPowerSpeed = (_currentAttackPower - _finalAttackPower) / _updatePowerSpeed;
    }
    public void ApplyDefenderPower(float value)
    {
        _currentDefenderPower = _defenderPower.value;
        _finalDefenderPower = value;
        _updateDefendPowerSpeed = (_currentDefenderPower - _finalDefenderPower) / _updatePowerSpeed;
    }
    public void ShowPowerPanel()
    {
        _powerPanel.SetActive(true);
    }
    private void Update()
    {
        UpdateAttackPowerSlider();
        UpdateDefenderPowerSlider();
    }
    private void UpdateAttackPowerSlider()
    {
        _currentAttackPower -= _updateAttackPowerSpeed * Time.deltaTime;
        _attackPower.value = _currentAttackPower;
    }
    private void UpdateDefenderPowerSlider()
    {
        _currentDefenderPower -= _updateDefendPowerSpeed *Time.deltaTime;
        _defenderPower.value = _currentDefenderPower;
    }
}
