using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Basic Setting")]
    [SerializeField] private float _maxZoom;
    [SerializeField] private float _minZoom;
    [SerializeField] private float _zoomSpeed = 10;
    private Transform _transform;
    private Camera _camera;
    private float _currentZoomSize;
    private void Awake()
    {
        _transform = transform;
        _camera = GetComponent<Camera>();
        _currentZoomSize = _camera.orthographicSize;
    }
    public void SetPosition(Vector3 position)
    {
        position.x = position.x / 2 - 0.5f;
        position.y = position.y / 2 - 0.5f;
        position.z = _transform.position.z;
        _transform.position = position;
    }
    public void SetPosition(float x, float y)
    {
        var position = _transform.position;
        position.x = x / 2 - 0.5f;
        position.y = y / 2 - 0.5f;
        _transform.position = position;
    }
    public void Zoom(float zoomAddIn)
    {
        _currentZoomSize -= zoomAddIn * _zoomSpeed;
        if(_currentZoomSize > _maxZoom)
        {
            _currentZoomSize = _maxZoom;
        }
        if(_currentZoomSize < _minZoom)
        {
            _currentZoomSize = _minZoom;
        }
        _camera.orthographicSize = _currentZoomSize;
    }
    public void Drag(Vector3 mousePosition)
    {
        var position = _transform.position;
        position.x += mousePosition.x;
        position.y += mousePosition.y;
        _transform.position = position;
    }
}
